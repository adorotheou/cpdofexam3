package com.agiletestingalliance;

public class MinMax {

    public int findTheBigger(int number1, int number2) {
        if (number1 > number2){
            return number1;
        }
        else{
            return number2;
        }
    }

}
