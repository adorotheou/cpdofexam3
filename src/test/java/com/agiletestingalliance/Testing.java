package com.agiletestingalliance;


import org.junit.*;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.util.logging.Logger;
import com.agiletestingalliance.Duration;
import com.agiletestingalliance.MinMax;
import com.agiletestingalliance.Usefulness;
import com.agiletestingalliance.AboutCPDOF;

public class Testing{

 	public Duration duration = new Duration();
 	public AboutCPDOF about = new AboutCPDOF();
	public MinMax minMax = new MinMax();	
	public Usefulness usefulness = new Usefulness();

	@Test	
      public void testCPDOF() {
          assertEquals("Testin cpdof", about.desc().length()>0, true);

      }

      	@Test	
      public void testDuration() {
          assertEquals("Testin duration", duration.dur().length()>0, true);

      }

       	@Test	
      public void testMinMax() {
            int number1 = 3;
            int number2 = 4;
          assertEquals("Testin minmax", minMax.findTheBigger(number1,number2), number2);
          assertEquals("Testin minmax", minMax.findTheBigger(number2,number1), number2);

      }
    	@Test	
      public void testUsefulness() {
             assertEquals("Testin usefulness", usefulness.desc().length()>0, true);

      }

  
  }